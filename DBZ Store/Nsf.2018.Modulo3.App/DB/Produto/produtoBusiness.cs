﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class produtoBusiness
    {
        public int Salvar(produtoDTO dto)
        {
            produtoDatabase db = new produtoDatabase();
            return db.Salvar(dto);
        }

        public void Alterar(produtoDTO dto)
        {
            produtoDatabase db = new produtoDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            produtoDatabase db = new produtoDatabase();
            db.Remover(id);
        }

        public List<produtoDTO> Consultar(string produto)
        {
            produtoDatabase db = new produtoDatabase();
            return db.Consultar(produto);
        }

        public List<produtoDTO> Listar()
        {
            produtoDatabase db = new produtoDatabase();
            return db.Listar();
        }
    }
}
