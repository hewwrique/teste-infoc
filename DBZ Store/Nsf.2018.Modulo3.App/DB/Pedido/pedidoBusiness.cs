﻿using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class pedidoBusiness
    {
        public int Salvar(pedidoDTO pedido, List<produtoDTO> produtos)
        {
            pedidoDatabase pedidoDatabase = new pedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            pedidoItemBusiness itemBusiness = new pedidoItemBusiness();
            foreach (produtoDTO item in produtos)
            {
                pedidoItemDTO itemDto = new pedidoItemDTO();
                itemDto.IdPedido = idPedido;
                itemDto.IdProduto = item.Id;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }

        public List<PedidoConsultarView> Consultar(string cliente)
        {
            pedidoDatabase pedidoDatabase = new pedidoDatabase();
            return pedidoDatabase.Consultar(cliente);
        }
    }
}
